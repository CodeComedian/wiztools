#! /bin/bash

AU_WORKDIR=au

CODEDIR=$HOME/$AU_WORKDIR/Main/src
SCRIPTDIR=$HOME/$AU_WORKDIR/scripts
PATCHDIR=$HOME/$AU_WORKDIR/patches
PRECOMPILE_PATCH_LIST=$SCRIPTDIR/precompile_patches

# set -x

apply_patches() {
    set -x
    patches=( `cat "${PRECOMPILE_PATCH_LIST}"` )
    for p in "${patches[@]}"
    do
        patch -p0 < $PATCHDIR/$p
    done
    set -x
}

main() {
    echo 'Setting up a new debug session from a pristine repository'
    if [[ -d "$CODEDIR" ]]; then
        cd $CODEDIR
        SVN_RESULT=$(svn status -q);
        if [[ -z "${SVN_RESULT}" ]]; then
            make clean
            apply_patches
            make etags
            $SCRIPTDIR/build_and_run.sh
        else
            echo "The svn repository is not pristine."
            set -x
            svn status -q
        fi
    else
        echo "Check the variable AU_WORKDIR in this script."
    fi
}

main
