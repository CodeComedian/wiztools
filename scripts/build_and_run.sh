#! /bin/bash

AU_WORKDIR=au
CODEDIR=$HOME/$AU_WORKDIR/Main/src
RUNDIR=$HOME/$AU_WORKDIR/Main/area

# set -x

build() {
    cd ${CODEDIR}
    make etags
    make
}

restart() {
    ROM_PID=$(ps -ef | grep "src/rom" | grep -v grep | awk '{print $2}')
    if [[ -n "$ROM_PID" ]]; then
        echo "Killing current ROM ${ROM_PID}"
        kill ${ROM_PID}
    fi
    cd ${RUNDIR}
    sleep 2
    nohup ../src/rom 1234 &
    ROM_PID=$(ps -ef | grep "src/rom" | grep -v "defunct" | grep -v grep | awk '{print $2}')
    echo "Rom running with PID ${ROM_PID}."
}

main() {
    echo "Compiling and running the current ROM project."
    if [[ -d "$CODEDIR" ]]; then
        build
        restart
    else
        echo "Problem: Check the variable AU_WORKDIR in this script."
    fi
}

main
