#! /bin/bash

AU_WORKDIR=au

CODEDIR=$HOME/$AU_WORKDIR/Main/src
SCRIPTDIR=$HOME/$AU_WORKDIR/scripts
PATCHDIR=$HOME/$AU_WORKDIR/patches
PRECOMPILE_PATCH_LIST=$SCRIPTDIR/precompile_patches

# set -x

remove_patches() {
    echo '                                                      '
    echo 'If you see: Unreversed patch detected!  Ignore -R? [n]'
    echo '            Control-C to stop this script'
    echo '            Your precompiled patches are not present in the code.'
    echo '                                                      '
    set -x
    patches=( `cat "${PRECOMPILE_PATCH_LIST}"` )
    for p in "${patches[@]}"
    do
        patch -p0 -R < $PATCHDIR/$p
    done
    set -x
}

main() {
    echo 'Removing precompile patches and compiling to check integrity of new code.'
    if [[ -d "$CODEDIR" ]]; then
        cd $CODEDIR
        remove_patches
        make etags
        $SCRIPTDIR/build_and_run.sh
        echo 'Removed precompile patches.  Did the compile work and does the rom run?'
    else
        echo "Check the variable AU_WORKDIR in this script."
    fi
}

main
